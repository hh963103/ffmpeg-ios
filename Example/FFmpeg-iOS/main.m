//
//  main.m
//  FFmpeg-iOS
//
//  Created by KWANG HYOUN KIM on 12/05/2018.
//  Copyright (c) 2018 KWANG HYOUN KIM. All rights reserved.
//

@import UIKit;
#import "DemoAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DemoAppDelegate class]));
    }
}
