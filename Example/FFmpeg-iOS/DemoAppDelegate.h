//
//  DemoAppDelegate.h
//  FFmpeg-iOS
//
//  Created by KWANG HYOUN KIM on 12/05/2018.
//  Copyright (c) 2018 KWANG HYOUN KIM. All rights reserved.
//

@import UIKit;

@interface DemoAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
