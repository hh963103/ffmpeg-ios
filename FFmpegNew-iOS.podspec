#
# Be sure to run `pod lib lint FFmpeg-iOS.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'FFmpegNew-iOS'
  s.version          = '4.1'
  s.summary          = 'ffmpeg universal wrapper.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'ffmpeg universal wrapper'
  s.homepage         = 'https://gitlab.com/hh963103/ffmpeg-ios.git'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'KWANG HYOUN KIM' => 'hh963103@gmail.com' }
  s.source           = { :git => 'https://gitlab.com/hh963103/ffmpeg-ios.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'
  s.frameworks = 'AVFoundation', 'Accelerate', 'AudioToolbox', 'CoreAudio', 'CoreGraphics', 'CoreMedia', 'MediaPlayer', 'QuartzCore', 'VideoToolbox'
  s.libraries = 'iconv', 'bz2', 'z'
  s.vendored_frameworks = 'FFmpeg.framework'
end
